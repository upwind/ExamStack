<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%-- <%@taglib uri="spring.tld" prefix="spring"%> --%>
<base href="${baseUrl}">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
<meta name="apple-mobile-web-app-capable" content="yes">
<link rel="shortcut icon" href="${baseUrl}resources/images/favicon.ico" />
<link href="${baseUrl}resources/bootstrap/css/bootstrap-v3.min.css" rel="stylesheet">
<link href="${baseUrl}resources/font-awesome/css/font-awesome.min.css" rel="stylesheet">
<link href="${baseUrl}resources/css/style.css" rel="stylesheet">
		